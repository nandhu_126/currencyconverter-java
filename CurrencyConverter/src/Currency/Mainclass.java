package Currency;
class USDClass{
	float usdQty;

	public USDClass(float usdQty) {
		super();
		this.usdQty = usdQty;
	}
	
}
class ConverterClass extends USDClass{
	String destCurrency;

	public ConverterClass(float usdQty, String destCurrency) {
		super(usdQty);
		this.destCurrency = destCurrency;
	}
	public float convertCurrency(){
		float uQty=this.usdQty;
		String dCurrency=this.destCurrency;
		if(dCurrency.equals("EUR")){
			return uQty*0.81f;
		}
		else if(dCurrency.equals("INR")){
			return uQty*64.31f;
		}
		else if(dCurrency.equals("MYR")){
			return uQty*3.95f;
		}
		else if(dCurrency.equals("SGD")){
			return uQty*1.32f;
		}
		else if(dCurrency.equals("GBP")){
			return uQty*0.72f;
		}
		else{
			return uQty*1.26f;
		}
	}
	public void dispaly(){
		System.out.println("The CAD amount equivalent to "+(int)this.usdQty+" USD is :"+ convertCurrency());
	}
	
}
public class Mainclass {

	public static void main(String[] args) {
		ConverterClass convert=new ConverterClass(10,"CAD");
		convert.dispaly();

	}

}
